import {
  DockerComposeEnvironment,
  StartedDockerComposeEnvironment,
  Wait
} from 'testcontainers';


const startCompose = async () => {

  const compose = new DockerComposeEnvironment(
    '.',
    'docker-compose-mount-fs.yml',
  )
    .withWaitStrategy(
    'ubuntu',
      Wait.forLogMessage('Mount succeed'),
  );
  
  return await compose.up()
};

describe('mount-fs', () => {
  let startedCompose: StartedDockerComposeEnvironment;
  beforeAll(async () => {
    startedCompose = await startCompose();
  })

  afterAll(async () => {
    await startedCompose.down();
  })

  it('should log mount succeed', async () => { 
    const ubuntuContainer = startedCompose.getContainer('ubuntu');

    const logs = await ubuntuContainer.logs();
  
    const log = await new Promise((resolve, reject) => {
      logs.on('data', (data)=> resolve(data))
      logs.on('error', (err)=> reject(err))
    })
    
    expect(log).toBe('Mount succeed !')
  })
})