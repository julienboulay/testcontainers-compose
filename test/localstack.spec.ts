import {
  DockerComposeEnvironment,
  StartedDockerComposeEnvironment,
  Wait
} from 'testcontainers';


const startCompose = async () => {

  const compose = new DockerComposeEnvironment(
    '.',
    'docker-compose-localstack.yml',
  )
    .withWaitStrategy(
    'localstack',
      Wait.forLogMessage('Finished creating resources'),
  );
  
  return await compose.up()
};

describe('localstack', () => {
  let startedCompose: StartedDockerComposeEnvironment;
  beforeAll(async () => {
    startedCompose = await startCompose();
  })

  afterAll(async () => {
    await startedCompose.down();
  })
  
  it('should run init-resources.sh', async () => { 
    const ubuntuContainer = startedCompose.getContainer('localstack');

    const logs = await ubuntuContainer.logs();
  
    const finishLog = await new Promise<string>((resolve, reject) => {
      logs.on('data', (data) => {
        if (data.toString().includes('Finished creating resources.')) {
          resolve(data.toString());
        }
      })
      logs.on('error', (err)=> reject(err))
    })  
    
    expect(finishLog).toContain('Finished creating resources.');
  })
})