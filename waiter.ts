export class Waiter {
  private timeout: any
  constructor() {
    this.waitLoop()
  }
  private waitLoop():void {
    this.timeout = setTimeout(() => { this.waitLoop() }, 100 * 1000)
  }
  okToQuit():void {
    clearTimeout(this.timeout)
  }
}